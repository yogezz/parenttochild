import { Component } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css'],
})
export class ParentComponent {
  count: number = 0;
  Increasecount() {
    this.count++;
  }
  Decreasecount() {
    this.count--;
  }
}
